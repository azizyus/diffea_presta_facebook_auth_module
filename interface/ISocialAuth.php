<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 28.05.2018
 * Time: 23:37
 */

interface ISocialAuth
{

    /**
     * ISocialAuth constructor.
     * @param $context GOT DAMN PRESTA CONTEXT
     */
    public function __construct($context);

    public function type();
    public function getId();
    public function requestUserData();
    public function getTitle();
    public function getEmail();
    public function getFirstName();
    public function getLastName();
    public function getAge();
    public function getBirthDate();
    public function getGender();
    public function getUser();
    public function getCallbackUrl();
    public function getLoginUrl();


    public function getViewVariables();
    public function getOptions();
    public function getControllerName();
}