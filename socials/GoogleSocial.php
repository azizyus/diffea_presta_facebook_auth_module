<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 28.06.2018
 * Time: 14:38
 */



include_once __DIR__."/../interface/ISocialAuth.php";
include_once __DIR__."/../queries/ConfigurationWrapper.php";
class GoogleSocial implements ISocialAuth
{


    public $google;
    public $email;
    public $id;
    public $callbackUrl=null;
    protected $user;
    public function __construct($context)
    {


        $this->callbackUrl = $context->link->getModuleLink("diffeafacebookmodule",'google');

        $google = new Google_Client();
        $google->setClientId(ConfigurationWrapper::getConfigurationValueIfExists("google_plus_client_id","google_plus_client_id"));
        $google->setClientSecret(ConfigurationWrapper::getConfigurationValueIfExists("google_plus_client_secret","google_plus_client_secret"));
        $google->setApplicationName("Kopya İçerik Google Auth TEST");
        $google->addScope(Google_Service_Oauth2::USERINFO_EMAIL);
        $google->setRedirectUri($this->callbackUrl);
        $this->google = $google;


    }

    public function type()
    {
        return 1;
        // TODO: Implement type() method.
    }

    public function getId()
    {
        // TODO: Implement getId() method.
        return $this->user->id;

    }

    public function requestUserData()
    {
        // TODO: Implement requestUserData() method.



        $token = $this->google->fetchAccessTokenWithAuthCode($_GET["code"]);

        $this->google->setAccessToken($token);
        $client = new Google_Service_Oauth2($this->google);

        $this->user = $client->userinfo->get();

//        dd($this->user);


    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.

        return "socialGoogle";
    }

    public function getControllerName()
    {
        // TODO: Implement getControllerName() method.
        return "google";
    }

    public function getEmail()
    {
        // TODO: Implement getEmail() method.
        return $this->user->email;
    }

    public function getFirstName()
    {
        // TODO: Implement getFirstName() method.
        return $this->user->givenName;
    }

    public function getLastName()
    {
        // TODO: Implement getLastName() method.
        return $this->user->familyName;
    }

    public function getAge()
    {
        // TODO: Implement getAge() method.

    }

    public function getBirthDate()
    {
        // TODO: Implement getBirthDate() method.
        return null;
    }

    public function getGender()
    {
        // TODO: Implement getGender() method.
    }

    public function getUser()
    {
        // TODO: Implement getUser() method.
        return $this->user;
    }

    public function getCallbackUrl()
    {
        // TODO: Implement getCallbackUrl() method.
        return $this->callbackUrl;
    }


    public function getViewVariables()
    {
        // TODO: Implement getViewVariables() method.

        $data=[
            "googleLoginUrl" => $this->getLoginUrl(),
            "googleCallbackUrlRaw"=>$this->getCallbackUrl(),

        ];

        return $data;
    }

    public function getOptions()
    {
        // TODO: Implement getOptions() method.

        $data = [

            "google_plus_client_id"=>ConfigurationWrapper::getConfigurationValueIfExists("google_plus_client_id","google_plus_client_id"),
            "google_plus_client_secret"=>ConfigurationWrapper::getConfigurationValueIfExists("google_plus_client_secret","google_plus_client_secret"),


        ];

        return $data;

    }

    public function getLoginUrl()
    {

        return $this->google->createAuthUrl();


    }
}