<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 28.05.2018
 * Time: 23:40
 */

include_once __DIR__."/../interface/ISocialAuth.php";
include_once __DIR__."/../queries/ConfigurationWrapper.php";
class FacebookSocial implements ISocialAuth
{

    public $callbackUrl;

    public $fb;
    protected $user;

    public $helper=null;


    public function __construct($context)
    {






        ## FACEBOOK ##
//        $fb = new Facebook\Facebook([
//            'app_id' => '180942999279831', // Replace {app-id} with your app id
//            'app_secret' => '5c3c88decdf06fe96219e68342268388',
//            'default_graph_version' => 'v2.2',
//        ]);
//
        $fb = new Facebook\Facebook([
            'app_id' =>  ConfigurationWrapper::getConfigurationValueIfExists("appId","app_id"), // Replace {app-id} with your app id
            'app_secret' => ConfigurationWrapper::getConfigurationValueIfExists("appSecret","app_secret"),
            'default_graph_version' => 'v2.2',
        ]);

        $this->fb = $fb;


        $this->callbackUrl = $context->link->getModuleLink('diffeafacebookmodule', 'facebook');

        

        ## FACEBOOK ##

    }

    public function requestUserData()
    {





        $helper = $this->fb->getRedirectLoginHelper();
        if (isset($_GET['state'])) { $helper->getPersistentDataHandler()->set('state', $_GET['state']); }
        try {
            $accessToken = $helper->getAccessToken();
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
            // When Graph returns an error
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
            // When validation fails or other local issues
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }


        $this->helper = $helper;

        $response = $this->fb->get('/me?fields=id,name,first_name,last_name,email', $accessToken);
        $user = $response->getGraphUser();
        $this->user = $user;

        $this->Id = $user->getId();
        $this->firstName = $user->getFirstName();
        $this->lastName = $user->getLastName();
        $this->email = $user->getEmail();
        $this->birthDay = $user->getBirthday();

    }

    protected $Id;
    protected $firstName;
    protected $lastName;
    protected $email;
    protected $birthDay;

    public function type()
    {
        // TODO: Implement type() method.
        return 0;
    }

    public function getId()
    {
        // TODO: Implement Id() method.
        return $this->user->getId();
    }

    public function getTitle()
    {
        // TODO: Implement getTitle() method.

        return "socialFacebook";
    }

    public function getControllerName()
    {
        // TODO: Implement getControllerName() method.
        return "facebook";
    }

    public function getEmail()
    {
        // TODO: Implement getEmail() method.
        return $this->email;
    }

    public function getUser()
    {
        // TODO: Implement getUser() method.
        return $this->user;
    }

    public function getFirstName()
    {
        // TODO: Implement getName() method.
        return $this->firstName;
    }

    public function getLastName()
    {
        // TODO: Implement getSurname() method.
        return $this->lastName;
    }

    public function getAge()
    {
        // TODO: Implement getAge() method.

    }

    public function getBirthDate()
    {
        // TODO: Implement getBirthDate() method.
        return $this->birthDay;
    }

    public function getGender()
    {
        // TODO: Implement getGender() method.
    }

    public function getCallbackUrl()
    {
        // TODO: Implement getCallbackUrl() method.
        return $this->callbackUrl;
    }

    public function getViewVariables()
    {
        // TODO: Implement getLinks() method.

        $data=[

            "facebookLoginUrl" => $this->getLoginUrl(),
            "facebookCallbackUrlRaw"=>$this->getCallbackUrl(),

        ];


        return $data;

    }

    public function getOptions()
    {
        // TODO: Implement getOptions() method.

        $options = [
            "appId"=>ConfigurationWrapper::getConfigurationValueIfExists("appId","app-id"),
            "appSecret"=>ConfigurationWrapper::getConfigurationValueIfExists("appSecret","app-secret"),

        ];


        return $options;

    }

    public function getLoginUrl()
    {


        $helper = $this->fb->getRedirectLoginHelper();
        $permissions = ['email']; // Optional permissions
        $loginUrl = $helper->getLoginUrl($this->callbackUrl, $permissions);

        // TODO: Implement loginUrl() method.
        return $loginUrl;
    }
}