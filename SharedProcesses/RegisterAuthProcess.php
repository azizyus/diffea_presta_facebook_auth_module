<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 11.06.2018
 * Time: 06:05
 */

include_once __DIR__."/../queries/SocialAuthQueries.php";
include_once __DIR__."/../SharedProcesses/RegisterAuthProcess.php";
include_once __DIR__."/../SharedProcesses/SendNewPasswordEmail.php";
class RegisterAuthProcess
{
    protected $currentShopId;
    protected $socialAuth;
    protected $context;
    public function __construct(Int $currentShopId,ISocialAuth $socialAuth,$context)
    {

        $this->currentShopId = $currentShopId;
        $this->socialAuth = $socialAuth;
        $this->context = $context;

    }


    public function RegisterOrAuth()
    {

        $currentShopId = $this->currentShopId;
        $socialAuth = $this->socialAuth;
        $context = $this->context;


        $result = SocialAuthQueries::getSocialAuthUser($currentShopId,$socialAuth);

        //user already registered with facebook so we dont need to REGISTER him just auth it
        if($result)
        {


            $customerId = $result["customerId"];

            $customerFromDbByRawQuery = SocialAuthQueries::isCustomerReallyExistById($customerId);


            //user has account so we can auth it
            if($customerFromDbByRawQuery!=null)
            {

//                print_r($customerSaved);
                SocialAuthQueries::authCustomerArray($customerFromDbByRawQuery,$context);



            }
            else
            {
                //its already registered but probably deleted account so register and auth

                $customerSaved = SocialAuthQueries::createCustomer($socialAuth);
                SocialAuthQueries::updateOldSocialTableRow($customerSaved,$socialAuth);
                SocialAuthQueries::authCustomer($customerSaved,$context);




            }




//            echo "VAR";
        }
        else //user didnt auth with this facebook accout so we need to register him
        {
//            echo "YOK";





//        echo "<br>";

            ### CHECK CUSTOMER EMAIL IS ALREADY EXIST ###

            $customerFromDbByRawQuery = SocialAuthQueries::isCustomerReallyExistByEmail($socialAuth->getEmail());


            ### CHECK CUSTOMER EMAIL IS ALREADY EXIST ###

            $sendNewPasswordEmail = new SendNewPasswordEmail();
            //cant find email so we can register him
            if($customerFromDbByRawQuery==null)
            {
                ### SAVE USER ###
                $password = uniqid();
                $customerSaved = SocialAuthQueries::createCustomer($socialAuth,$password);

                ### SAVE USER ###


                ## AUTH USER ##

                SocialAuthQueries::authCustomer($customerSaved,$context);

                ## AUTH USER ##


                ## INSERT TO SOCIAL TABLE ##
                SocialAuthQueries::saveCustomerToSocialAuthTable($customerSaved,$socialAuth);
                ## INSERT TO SOCIAL TABLE ##

                ## NEW PASS EMAIL INSTANCE ##
                $sendNewPasswordEmail->setEmail($customerSaved->email);
                $sendNewPasswordEmail->setPassword($password);

            }
            else //attach email to account
            {

                SocialAuthQueries::saveCustomerToSocialAuthTableArray($customerFromDbByRawQuery,$socialAuth);

                SocialAuthQueries::authCustomerArray($customerFromDbByRawQuery,$context);

//                $sendNewPasswordEmail->setEmail($customerFromDbByRawQuery["email"]);
//                $sendNewPasswordEmail->setPassword($customerFromDbByRawQuery["password"]);

            }


            $sendNewPasswordEmail->sendEmail();







        }

        ### FACEBOOK ###
    }

}