<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 24.09.2018
 * Time: 03:48
 */

class RedirectAfterAuth
{

    public $key="redirectAfterAuth";
    public function __construct()
    {



    }



    public function setUrlToSession()
    {


        $url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
        $count = 0;
        str_replace("module","_REPLACED_",$url,$count);
        if($count==0) $_SESSION[$this->key] = $url;
    }

    public function getUrlFromSession()
    {

        $url = $_SESSION[$this->key];

        if($url==null) return "";
        else return $url;

    }



}