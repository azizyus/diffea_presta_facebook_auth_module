<?php


include_once __DIR__."/../../vendor/autoload.php";
include_once __DIR__."/../../queries/SocialAuthQueries.php";
include_once __DIR__."/../../SharedProcesses/RegisterAuthProcess.php";
include_once __DIR__."/../../SharedProcesses/RedirectAfterAuth.php";
class diffeafacebookmodulegoogleModuleFrontController extends ModuleFrontController
{
    public function initContent()
    {

        if(!session_id()) {
            session_start();
            session_unset();
        }

        $currentShopId = $this->context->shop->id;


//
//        //GET USER DATA
        $socialAuth = new GoogleSocial($this->context);
        $socialAuth->requestUserData();


//
//        //AUTH OR REGISTER USER WITH SPECIFIED AUTH AND SHOP ID + CONTEXT
        $registerAuthProcess = new RegisterAuthProcess($currentShopId,$socialAuth,$this->context);
        $registerAuthProcess->RegisterOrAuth();



        parent::initContent();
        $this->setTemplate('module:mymodule/views/templates/front/social.tpl');

        ## LAST URL ##
        $redirectAfterAuth = new RedirectAfterAuth();
        $url = $redirectAfterAuth->getUrlFromSession();
        ## LAST URL ##

        Tools::redirect($url);

    }
}