<?php


class ConfigurationWrapper
{


    public static function getConfigurationValueIfExists($key,$default)
    {

        $configurationValue = \PrestaShop\PrestaShop\Adapter\Entity\Configuration::get($key);



        if($configurationValue!=null) return $configurationValue;


        return $default;

    }

}