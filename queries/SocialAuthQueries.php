<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 10.06.2018
 * Time: 03:06
 */

class SocialAuthQueries
{


    public static function getSocialAuthUser($currentShopId,ISocialAuth $facebook)
    {

        $result = Db::getInstance()->getRow(

            "SELECT customerId
            FROM  "._DB_PREFIX_."social_auth
           WHERE shopId = $currentShopId && socialKey='{$facebook->getId()}'  && type={$facebook->type()}"

        );

        return $result;

    }


    public static function isCustomerReallyExistById(Int $id)
    {

        $customer = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."customer WHERE id_customer = ".$id);

        return $customer;

    }

    public static function isCustomerReallyExistByEmail(String $email)
    {

        $customer = Db::getInstance()->getRow("SELECT * FROM "._DB_PREFIX_."customer WHERE email ='".$email."' ");

        return $customer;

    }

    public static function createCustomer(ISocialAuth $socialAuth,$password=null)
    {
        $customer = new Customer();




        $customer->email = $socialAuth->getEmail();
        $customer->firstname = $socialAuth->getFirstName() == null ? "FirstName" : $socialAuth->getFirstName();
        $customer->lastname = $socialAuth->getLastName() == null ? "LastName" : $socialAuth->getLastName();
        $customer->logged = 1;
        $customer->birthday = $socialAuth->getBirthDate();
        if($password==null) $customer->passwd = md5(_COOKIE_KEY_.uniqid());
        else $customer->passwd = md5(_COOKIE_KEY_.$password);


        $customer->save();

        return $customer;

    }

    public static function authCustomerArray($customerArray,$context)
    {
        $context->cookie->id_customer = intval($customerArray["id_customer"]);
        $context->cookie->customer_lastname = $customerArray["lastname"];
        $context->cookie->customer_firstname = $customerArray["firstname"];
        $context->cookie->logged = 1;
        $context->cookie->passwd = $customerArray["passwd"];
        $context->cookie->email = $customerArray["email"];
        if (Configuration::get('PS_CART_FOLLOWING') AND (empty($context->cookie->id_cart) OR Cart::getNbProducts($context->cookie->id_cart) == 0))
            $context->cookie->id_cart = intval(Cart::lastNoneOrderedCart(intval($customerArray["id"])));


    }

    public static function updateOldSocialTableRow(Customer $newCustomer,ISocialAuth $oldSocialAuth)
    {


        Db::getInstance()->execute("UPDATE "._DB_PREFIX_."social_auth SET customerId={$newCustomer->id} WHERE type={$oldSocialAuth->type()} AND socialKey='".$oldSocialAuth->getId()."'");

    }

    public static function authCustomer(Customer $customerSaved, $context)
    {
        $context->cookie->id_customer = intval($customerSaved->id);
        $context->cookie->customer_lastname = $customerSaved->lastname;
        $context->cookie->customer_firstname = $customerSaved->firstname;
        $context->cookie->logged = 1;
        $context->cookie->passwd = $customerSaved->passwd;
        $context->cookie->email = $customerSaved->email;
        if (Configuration::get('PS_CART_FOLLOWING') AND (empty($context->cookie->id_cart) OR Cart::getNbProducts($context->cookie->id_cart) == 0))
            $context->cookie->id_cart = intval(Cart::lastNoneOrderedCart(intval($customerSaved->id)));
    }

    public static function saveCustomerToSocialAuthTable(Customer $customer,ISocialAuth $ISocialAuth)
    {


        Db::getInstance()->execute('INSERT INTO `ps_social_auth`(`id`, `customerId`, `shopId`, `socialKey`, `socialToken`, `type`) VALUES (null,'.$customer->id.','.$customer->id_shop.',"'.$ISocialAuth->getId().'",null,'.$ISocialAuth->type().')');
            



    }
    public static function saveCustomerToSocialAuthTableArray($customer,ISocialAuth $ISocialAuth)
    {


        Db::getInstance()->execute('INSERT INTO `ps_social_auth`(`id`, `customerId`, `shopId`, `socialKey`, `socialToken`, `type`) VALUES (null,'.$customer["id_customer"].','.$customer["id_shop"].',"'.$ISocialAuth->getId().'",null,'.$ISocialAuth->type().')');




    }
}