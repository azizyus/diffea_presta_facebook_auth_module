<?php
/**
 * Created by PhpStorm.
 * User: dd
 * Date: 29.06.2018
 * Time: 18:20
 */

class Auths
{


    public $auths=[];


    public function __construct($context)
    {



        $this->auths[]=new GoogleSocial($context);
        $this->auths[]=new FacebookSocial($context);




    }


    public function getOptions()
    {

        $data=[];

        foreach ($this->auths as $auth)
        {
            $data[]=$auth->getOptions();

        }


        return $data;
    }

}