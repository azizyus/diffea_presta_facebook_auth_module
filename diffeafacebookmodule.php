<?php
if (!defined('_PS_VERSION_'))
{
    exit;
}

include_once __DIR__."/vendor/autoload.php";
include_once __DIR__."/Compose/Auths.php";
include_once __DIR__."/socials/FacebookSocial.php";
include_once __DIR__."/socials/GoogleSocial.php";
include_once __DIR__."/SharedProcesses/RedirectAfterAuth.php";
class diffeafacebookmodule extends Module
{
    public $facebookAuthLink = null;
    public $googleAuthLink = null;
    public $auths=null;
    public function __construct()
    {
        $this->name = 'diffeafacebookmodule';
        $this->tab = 'front_office_features';
        $this->version = '1.0.0';
        $this->author = 'Diffea';
        $this->need_instance = 0;
        $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
        $this->bootstrap = true;
        


        parent::__construct();

        $this->displayName = $this->l('Diffea Facebook Auth Module');
        $this->description = $this->l("You'r customers can register with facebook");

        $this->confirmUninstall = $this->l('Are you sure you want to uninstall? (if you uninstall you facebook users will be deleted and you cannot undo this action)');

        if (!Configuration::get('MYMODULE_NAME'))
            $this->warning = $this->l('No name provided');








        if(!session_id()) {
            session_start();
        }




            ## IS LOGGED ##
             $isLogged=false;
               if($this->context->cookie->logged!=null) $isLogged=true;
            ## IS LOGGED ##

        if($isLogged==false)
        {
            ## LAST URL ##
            $redirectAfterAuth = new RedirectAfterAuth();
            $redirectAfterAuth->setUrlToSession();
//            echo $redirectAfterAuth->getUrlFromSession();
            ## LAST URL ##
        }

        $this->auths = new Auths($this->context);




        ## SHARE DATA WITH TEMPLATE ##

        $templateData = [];

        foreach ($this->auths->auths as $auth)
        {

            $templateData = array_merge($templateData,$auth->getViewVariables());

        }



        //do not override it
        $templateData["isLogged"]=$isLogged;

        $this->context->smarty->assign(
            $templateData
        );

        ## SHARE DATA WITH TEMPLATE ##


    }

    public function displayForm()
    {


        $template = $this->context->smarty->createTemplate(__DIR__."/views/templates/back/options.tpl");



        $languages = Language::getLanguages(false);

        $data=[];

        //app-id,secret etc...
        foreach ($this->auths->auths as $auth)
        {

            $data = array_merge($data,$auth->getOptions());

        }

        //facebook,google
        foreach ($languages as $language)
        {

            foreach ($this->auths->auths as $auth)
            {

            $data[$auth->getTitle()][]=$this->context->link->getModuleLink('diffeafacebookmodule', $auth->getControllerName(),[],true,$language["id_lang"]);
//            $data[$auth][]= $this->context->link->getModuleLink('diffeafacebookmodule','google',[],true,$language["id_lang"]);
            }
        }






        $template->assign($data);

        return $template->fetch();

    }

    public function getContent()
    {




        if (Tools::isSubmit("update_social_options"))
        {


            //save options
            foreach ($this->auths->auths as $auth)
            {

                foreach ($auth->getOptions() as $key => $option)
                {
                    Configuration::updateValue($key,Tools::getValue($key));
                }

            }
//            ## FACEBOOK ##
//
//            $appId = Tools::getValue("appId");
//            $appSecret = Tools::getValue("appSecret");
//            Configuration::updateValue("appId",$appId);
//            Configuration::updateValue("appSecret",$appSecret);
//
//            ## /FACEBOOK ##
//
//
//            ## GOOGLE ##
//            $clientId = Tools::getValue("google_plus_client_id");
//            $clientSecret = Tools::getValue("google_plus_client_secret");
//            Configuration::updateValue("google_plus_client_id",$clientId);
//            Configuration::updateValue("google_plus_client_secret",$clientSecret);
//            ## /GOOGLE ##



           $this->displayConfirmation("Your Changes Saved");
        }



        return $this->displayForm();

    }

    public function install()
    {





        if (Shop::isFeatureActive())
            Shop::setContext(Shop::CONTEXT_ALL);


        ## TABLE CREATE ##


        try
        {
            Db::getInstance()->execute("CREATE TABLE `"._DB_PREFIX_."social_auth` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `customerId` int(10) NOT NULL,
 `shopId` int(11) NOT NULL,
 `socialKey` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `socialToken` varchar(255) CHARACTER SET utf8 DEFAULT NULL,
 `type` int(10) NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_turkish_ci");
        }
        catch (Exception $exception)
        {

        }



        ## TABLE CREATE ##


        //generally it defines placement of login button or what you want to show in front side and where
        ## HOOKS ##

        return parent::install() &&
            $this->registerHook('displaySocialAuth') &&
//            $this->registerHook('header') &&
            Configuration::updateValue('MYMODULE_NAME', 'my friend');

        ## HOOKS ##


    }

    public function uninstall()
    {
        if (!parent::uninstall() ||
            !Configuration::deleteByName('MYMODULE_NAME')
        )
            return false;


       ## DELETE TABLE ##

        try
        {
            Db::getInstance()->execute("DROP TABLE `prestashop`.`"._DB_PREFIX_."social_auth`");
        }
        catch (Exception $exception)
        {

        }

       ## DELETE TABLE ##

        return true;
    }

    public function hookDisplaySocialAuth($params)
    {
        return $this->display(__FILE__, 'social.tpl');
    }

    public function hookDisplayHeader()
    {
        $this->context->controller->addCSS($this->_path.'css/mymodule.css', 'all');
    }


}